# Ansible for PICC

### 前置步骤

本文档的所有操作，均需要基于以下几点：

1. 目标 Linux 是 SUSE 11

2. 目标 Linux 已经安装了 JDK 1.5 和 Weblogic 9.2.3

3. 目标 Linux 已经完成了基本的 Weblogic 安装配置

对于基本安装配置，请参考附件中的 **PICC 安装手册**。

### Playbook 的说明

1. adminserver.yml 用于按照 **PICC 安装手册** 创建主控机器，包含3个步骤（创建用户，创建 Domain, 配置主控 Domain)

2. managedserver.yml 用于按照 **PICC 安装手册** 创建受管机器，同样包含3个步骤（创建用户，创建 Domain, 配置受管 Domain)

3. deploy.yml 用于在主控上向受管部署一个安装包

4. increupdate.yml 用于增量更新一个软件包

5. fallback.yml 用于回滚一个软件包

6. clearcache.yml 用于清空受管机器上的软件缓存 （ 一般不用 )

### 操作步骤准备

1. 在本机中，将 *ansible* 目录下的所有文件，拷贝到 /etc/ansible/ 目录下

2. 修改 /etc/ansible/hosts 文件，如下所示:

    ```bash
    [AdminServer]
    123.123.123.123 ansible_ssh_pass=123456
    
    [ManagedServer]
    123.123.123.123 ansible_ssh_pass=123456
    ```
    
    将 IP 地址改为主控和受管的 IP 地址，并将密码改为 SUSE 上的 root 密码
    
3. 修改 /etc/ansible/group_vars/ManagedServer.yml 中 admin_ip 值，更改为主控的 IP

### 实际运行

1. 安装主控

    ```bash
    ansible-playbook -s /etc/ansible/adminserver.yml
    ```
    
    此时，从 *http://主控 IP:7001/console* 中应该能够看到 Weblogic 的管理员界面
    
2. 安装受管
    
    ```bash
    ansible-playbook -s /etc/ansible/managedserver.yml
    ```
    
    此时，从 *http://受管 IP:7002/* 中应该能够看到 Weblogic 的运行界面

3. 部署软件包

    拷贝附件中的文件 **picc.war** ，到 /etc/ansible/roles/prepare_package/files/ 目录下，并运行
    
    ```bash
    ansible-playbook -s /etc/ansible/deploy.yml
    ```
    
    此时，从 *http://受管 IP:7002/picc* 中应该能够看到最终的软件界面
    
### 源码说明

PICC 的操作源代码，都在 roles 里面，说明如下：

- clear_cache 清空缓存
- configure_domain 配置主控
- create_domain 创建主控
- create_managed 创建受管
- create_user 创建用户
- deploy_package 部署软件包
- fallback_package 回滚软件包
- incremental_update 增量更新软件包
- prepare_package 远程拷贝软件包

**注意：** 配置主控中的操作步骤，均按照 **PICC 安装手册** 中的说明来操作，start_admin.sh 脚本的写法略有不同。
