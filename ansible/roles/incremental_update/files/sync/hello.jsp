<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>PICC Incremental Update</title>
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="./vendor/material.css">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <div class="demo-layout mdl-layout mdl-layout--fixed-header mdl-js-layout mdl-color--grey-100">
        <header class="demo-header mdl-layout__header mdl-layout__header--scroll mdl-color--grey-100 mdl-color-text--grey-800">
            <div class="mdl-layout__header-row">
                <span class="mdl-layout-title">PICC Incremental Update</span>
            </div>
        </header>
        <div class="demo-ribbon" style="background-color: #795548"></div>

        <main class="demo-main mdl-layout__content">
            <div class="demo-container mdl-grid">
                <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
                <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">
                    <h3>PICC Incremental Update hello.jsp</h3>
                    <p>
                        <%= new String("hello incremental jsp!") %>
                    </p>
                    <p><a href="index.html">Back to Index</a></p>
                </div>
            </div>
        </main>
    </div>
    <script src="./vendor/material.js"></script>
</body>

</html>
